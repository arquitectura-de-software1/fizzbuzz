import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class FizzBuzz_Test {

	@Test
	void Return_Fizz_with_number_3() {
		FizzBuzz fizzbuzz = new FizzBuzz();
		assertEquals("Fizz", fizzbuzz.generate(3));
	}
	@Test
	void Return_Buzz_with_number_5() {
		FizzBuzz fizzbuzz = new FizzBuzz();
		assertEquals("Buzz", fizzbuzz.generate(5));
	}
	@Test
	void Return_FizzBuzz_with_number_15() {
		FizzBuzz fizzbuzz = new FizzBuzz();
		assertEquals("FizzBuzz", fizzbuzz.generate(15));
	}
	@Test
	void Return_FizzBuzz_with_number_30() {
		FizzBuzz fizzbuzz = new FizzBuzz();
		assertEquals("FizzBuzz", fizzbuzz.generate(30));
	}
	@Test
	void Return_58_with_number_58() {
		FizzBuzz fizzbuzz = new FizzBuzz();
		assertEquals("58", fizzbuzz.generate(58));
	}
	@Test
	void Return_FizzBuzz_with_number_75() {
		FizzBuzz fizzbuzz = new FizzBuzz();
		assertEquals("FizzBuzz", fizzbuzz.generate(75));
	}
	@Test
	void Return_83_with_number_83() {
		FizzBuzz fizzbuzz = new FizzBuzz();
		assertEquals("83", fizzbuzz.generate(83));
	}
	@Test
	void Return_Buzz_with_number_100() {
		FizzBuzz fizzbuzz = new FizzBuzz();
		assertEquals("Buzz", fizzbuzz.generate(100));
	}
	@Test
	void Return_100_cases_when_100_numbers_set() {
		FizzBuzz fizzbuzz = new FizzBuzz();
		assertEquals(100, fizzbuzz.generate_numbers(100));
	}
	@Test
	void Return_50_cases_when_50_numbers_set() {
		FizzBuzz fizzbuzz = new FizzBuzz();
		assertEquals(50, fizzbuzz.generate_numbers(50));
	}
	@Test
	void Return_1_cases_when_1_numbers_set() {
		FizzBuzz fizzbuzz = new FizzBuzz();
		assertEquals(1, fizzbuzz.generate_numbers(1));
	}
	@Test
	void Return_15_cases_when_15_numbers_set() {
		FizzBuzz fizzbuzz = new FizzBuzz();
		assertEquals(15, fizzbuzz.generate_numbers(15));
	}
}