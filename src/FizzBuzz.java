import java.util.Scanner;
public class FizzBuzz {
  public void initializate() {
	  int size;
	  Scanner var = new Scanner(System.in);
	  System.out.println("Cant de numeros: ");
	  size=var.nextInt();
	  var.close(); 
	  System.out.println("====RESULT====");
	  generate_numbers(size);
  }
  
  public String generate(int number) {
	  if (number % 3 == 0 && number % 5 == 0) {
		  return "FizzBuzz";
	  }
	  else {
		  if (number % 3 == 0) {
			  return "Fizz";
		  }
		  else {
			  if (number % 5 == 0) {
				  return "Buzz";
			  }
			  else
			  {  
				  return Integer.toString(number); 
			  }
		  }
	  }
	
  }
  
  public int generate_numbers(int size) {
	  int range = 0;
	  for(int num=0; num <= size; num++ ) {
		  System.out.println(num + " => " + generate(num));
		  range = num;
	  }
	  return range;
  }
}
